CREATE TABLE IF NOT EXISTS `auth` (
`id` INT PRIMARY KEY,
`token` TEXT
);

CREATE TABLE IF NOT EXISTS `profiles` (
`id` INT PRIMARY KEY,
`name` TEXT,
`surname` TEXT,
`middlename` TEXT,
`telephone` TEXT,
`role` INT,
FOREIGN KEY (id) REFERENCES auth (id)
);

CREATE TABLE IF NOT EXISTS `students` (
`id` INT PRIMARY KEY,
`group` TEXT,
FOREIGN KEY (id) REFERENCES auth (id)
);

CREATE TABLE IF NOT EXISTS `schoolers` (
`id` INT PRIMARY KEY,
`school` TEXT,
`group` TEXT,
FOREIGN KEY (id) REFERENCES auth (id)
);

CREATE TABLE IF NOT EXISTS `questions` (
`id` INT PRIMARY KEY AUTO_INCREMENT,
`date` DATE UNIQUE, 
`question` TEXT,
`answer` TEXT,
`value` INT(4)
);

CREATE TABLE IF NOT EXISTS `answers` ( 
`id` INT,
`question` INT,
`answer` TEXT,
`correct` TINYINT(1)
);