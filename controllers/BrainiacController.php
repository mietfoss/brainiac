<?php

namespace app\controllers;


use yii;
use app\models\Questions;
use app\models\Answers;
use yii\data\ActiveDataProvider;

class BrainiacController extends \yii\web\Controller
{
    public function actionIndex()
    {

        $question = Questions::getCurrentQuestion();

        if ($question != NULL) {

            if (Yii::$app->user->isGuest) {
                return $this->render('index-question', [
                    "question" => $question,
                    "content" => "TODO: add 'you need to register' text"
                ]);
            }

            $model = new Answers();

            if($model->load(Yii::$app->request->post())) {
                $model->id = Yii::$app->user->id;
                $model->question = $question->id;
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            }

            if ($model = Answers::findOne(['id'=>Yii::$app->user->id, 'question'=>$question->id])) { // if answered
                return $this->render('index-question', [
                    "question" => $question,
                    "content" => "TODO: add 'you have already answered' text"
                ]);
            }
            else {
                $model = new Answers();
                return $this->render('index-question', [
                    "question" => $question,
                    "content" => $this->renderPartial("form_submit", ["model" => $model])
                ]);

            }
        }
        else {
            return $this->render('index-empty');
        }
    }

    public function actionPanel()
    {
        return $this->render('panel');
    }

}