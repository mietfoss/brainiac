<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\authclient\clients\VKontakte;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Profile;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /* public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }*/

//TESTING VK AUTHORISATION

    public function actionLogin()
    {
    
        $oauthClient =  new VKontakte();
        //$url = $oauthClient->buildAuthUrl(); // Build authorization URL
        $url = 'https://oauth.vk.com/authorize?client_id=4575007&redirect_uri=http://fbmiet.ru/basic/web/index.php&display=popup';
        Yii::$app->getResponse()->redirect($url); // Redirect to authorization URL.
        // After user returns at our site:
        $code = $_GET['code'];
        $accessToken = $oauthClient->fetchAccessToken($code); // Get access token
    
    }

//

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionProfile()
    {
        $model = new Profile();

        $id = 0; //User's VK ID. If ID exist, view will demonstrate changing current profile. 
                //Else - adding new (just for demonstration)

        if ($model->load(Yii::$app->request->post()) && $model->profile($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        } else{
            return $this->render('/site/profile', [
                'model' => $model,
                'id' => $id
                ]);
        }
    }
}
