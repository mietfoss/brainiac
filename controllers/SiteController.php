<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Request;
use app\models\ContactForm;
use app\models\Profile;
use app\models\Login;
use app\models\Questions;
use app\models\Answers;
use \yii\authclient\OAuthToken;
 


class SiteController extends Controller
{
    

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $question = Questions::getCurrentQuestion();

        if ($question != NULL) {

            /*
            if ($model->load(Yii::$app->request->post())) {
                $model->id = Yii::$app->user->id;
                $model->question = $question->id;
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            } */

            if ($answer = Answers::findOne(['user_id'=>Yii::$app->user->id, 'question'=>$question->id])) { // if answered
                return $this->render('question-answered', [
                    "question" => $question,
                    "answer" => $answer,
                ]);
            }
            else {
                $answer = new Answers();
                if ($answer->load(Yii::$app->request->post())) { // if user is answering a question
                    if (!Yii::$app->user->isGuest) { // if user is logged in
                        $answer->user_id = Yii::$app->user->id;
                        $answer->question = $question->id;
                        if ($answer->save()) {
                            return $this->actionIndex();
                        }
                        else {
                            return $this->actionProfile();
                        }
                    }
                    else {
                        return $this->actionProfile();
                    }
                }
                return $this->render('question', [
                    "question" => $question,
                    "answer" => $answer,
                    "answered" => false,
                ]);

            }
        }
        else {
            return $this->render('index');
        }
    }


    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new Login();
        if (isset($_GET['code'])) {
            $code = $_GET['code'];
            $token = $model->getToken($code);

            $id = $token->getParam('user_id');
            $tok = $token->getToken();
            $model->login($id,$tok);
            return $this->goBack();
        }
        else {
            return $model->getCode();
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionProfile()
    {
        if(Yii::$app->user->isGuest) {
            return $this->actionLogin();
        }
        $id = Yii::$app->user->identity->id;
        $model = new Profile();
        if ($model->load(Yii::$app->request->post()) && $model->profile($id)) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        } else{
            return $this->render('/site/profile', [
                'model' => $model,
                'id' => $id
                ]);
        }
    }
}
