<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\Profile */

$this->title = 'User profile';
// id should be in the OAuth controller. 
// TODO: make algorithm to find user's ID in 'auth' table while he logs in
// and take users profile with this ID

$connection = Yii::$app->db;
$command = $connection->createCommand('SELECT * FROM profiles WHERE id='.(string)$id); //Now it's hardcoded
$post = $command->queryOne();
//
?>
<div class="site-profile">
	<div class="row">
	        <div class="col-lg-5">
	            <?php $form = ActiveForm::begin(['id' => 'profile']); ?>
	                <?= $form->field($model, 'name')->textInput(['value'=>$post['name']]) ?>
	                <?= $form->field($model, 'surname')->textInput(['value'=>$post['surname']]) ?>
	                <?= $form->field($model, 'middlename')->textInput(['value'=>$post['middlename']]) ?>
	                <?= $form->field($model, 'telephone')->textInput(['value'=>$post['telephone']]) ?>  
	                <?= $form->field($model, 'role')->dropDownList(array('1'=>'Школьник',
	                											'2'=>'Студент'))?>
	                <div class="form-group">
	                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'profile-button']) ?>
	                </div> 
	            <?php ActiveForm::end(); ?>
	        </div>
	    </div>
</div>
