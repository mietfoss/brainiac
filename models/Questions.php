<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%questions}}".
 *
 * @property integer $id
 * @property string $date
 * @property string $question
 * @property string $answer
 * @property integer $value
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questions}}';
    }


    /**
     * Finds and returns current day's
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getCurrentQuestion() {
        return Questions::find()
            ->where("(`date` = CURDATE() AND CURTIME() >= '18:00') OR (`date` = DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND CURTIME() < '18:00')")
            ->one();
    }

    /**
     * Returns specific date's question.
     *
     * @param $date - Date in YYYY-MM-DD format
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getQuestion($date) {
        return Questions::find()
            ->where(['date' => $date])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['date'], 'safe'],
            [['text', 'answer'], 'string'],
            [['value'], 'integer'],
            [['date'], 'unique'],
            [['date'], 'date', 'format' => 'yyyy-M-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Дата'),
            'question' => Yii::t('app', 'Текст'),
            'text' => Yii::t('app', 'Текст вопроса'),
            'answer' => Yii::t('app', 'Правильный ответ'),
            'value' => Yii::t('app', 'Стоимость'),
        ];
    }
}