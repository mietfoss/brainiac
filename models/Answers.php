<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%answers}}".
 *
 * @property integer $id
 * @property integer $question
 * @property string $answer
 * @property integer $correct
 */
class Answers extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%answers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'question', 'correct'], 'integer'],
            [['answer'], 'string'],
            [['answer'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'question' => Yii::t('app', 'Question'),
            'answer' => Yii::t('app', 'Answer'),
            'correct' => Yii::t('app', 'Correct'),
        ];
    }
}