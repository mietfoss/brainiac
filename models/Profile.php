<?php

namespace app\models;


use Yii;
use yii\base\Model;
use yii\db\Command;

class Profile extends Model

{
    public $name;
    public $surname;
    public $middlename;
    public $telephone;
    public $role;

	public function rules(){
		return [
			[['name','surname','middlename','telephone','role'], 'required'],
			[['name','surname','middlename','telephone'], 'trim'],
			['telephone','integer'],
			['role','integer'],
			[['name','surname','middlename'],'string','length' => [2,30]],
		];
	}

	public function attributeLabels()
	    {
	        return [
	            'name' => 'Имя',
	            'surname' => 'Фамилия',
	            'middlename' => 'Отчество',
	            'telephone' => 'Телефонный номер',
	            'role' => 'Школьник/студент'
	        ];
	    }

	public function profile($id){
		
		if($this->validate()){
			$connection = Yii::$app->db;
				$connection -> createCommand() -> update('profiles',[
						'name'=> $this->name,
						'surname' => $this->surname,
						'middlename' => $this->middlename,
						'telephone' => $this->telephone,
						'role' => $this->role,
				],'id='.(string)$id) -> execute(); 
			return true;
		} else {
			return false;
		}
	}
} 

