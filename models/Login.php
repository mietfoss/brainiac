<?php

namespace app\models;
use Yii;
use yii\authclient\clients\VKontakte;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\Request;
use app\models\User;
use yii\helpers\Url;
use \app\controllers\SiteController;
class Login 
{

    public $oauthClient;

    public function __construct(){
        $this->oauthClient = new VKontakte();
    }

    public function fillBase($id, $token){
        $connection = Yii::$app->db;

        $model = $connection->createCommand("SELECT * FROM auth where id=".$id);
        $dbrecord = $model->queryOne();
        //var_dump($dbrecord);
        if(!($dbrecord)){
            $connection -> createCommand() -> insert('auth',[
                'id' => $id]) -> execute();
            $connection -> createCommand() -> insert('profiles',[
                'id' => $id]) -> execute(); 
        }
        $connection -> createCommand() -> update('auth',[
                        'accessToken'=> $token,
                ],'id='.(string)$id) -> execute();
        //var_dump($token);
    }


    public function getToken($code){
        $oauthClient = $this->oauthClient;

        $params = [
            'client_id' => 4575007,
            'client_secret' => 'ky3GpCc8GufP4B3b85YP',
            'grant_type' => 'authorization_code',
            'redirect_uri' => Url::to(['/site/login'], true),
        ];
        return $oauthClient->fetchAccessToken($code, $params);
        
    }


    public function getCode(){
        $oauthClient = $this->oauthClient;

        $params = [
            'client_id' => 4575007,
            'response_type' => 'code',
            'redirect_uri' => Url::to(['/site/login'], true),
            'xoauth_displayname' => '',
            'display' => 'popup',
        ];

        $url = $oauthClient->buildAuthUrl($params); // Build authorization URL
        return Yii::$app->getResponse()->redirect($url); // Redirect to authorization URL.
    }
    
    public function login($id,$token)
    {
        $this->fillBase($id, $token);
        //$string = (string)$id."|".(string)$token;
        $identity = User::findIdentity($id);

        $result = Yii::$app->user->login($identity, 3600);
        Yii::$app->session->open();
        if ($token == NULL) {
            Yii::$app->session['token'] = "TOKEN IS NULL";
        } else {
            Yii::$app->session['token'] = "TOKEN IS NOT NULL";
//            Yii::$app->session->set('token', $token);
        }
        return $result;

    }



}


