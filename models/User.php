<?php

namespace app\models;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    //public  $id;
    //public static $accessToken;
    /*

    public function __construct($id,$token){
    	$this->id = $id;
    	$this->accessToken = $token;
    }
	*/

	public static function tableName()
    {
        return 'auth';
    }

    public static function findIdentity($id)
    {
       	return static::findOne($id);       
	}

    public static function findIdentityByAccessToken($id, $type = null)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->id;
    }

    public function validateAuthKey($authKey)
    {
        return true;
    }
}