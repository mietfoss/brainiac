<?php

namespace app\modules\manager\controllers;

use Yii;
use yii\web\Controller;
use app\models\Answers;
use app\models\Questions;
class UsersController extends Controller
{
    public function actionIndex()
    {
        $answer = Answers::find()->where('`correct` is NULL')->one();

        if (Yii::$app->request->post() && $answer->load(Yii::$app->request->post()) && $answer->save()) {
            return $this->redirect(['index']);
        }
        if (!($answer)) {
            return $this->render('index-empty', ['answer' => $answer]);
        }
        $question = Questions::find()->where(['id'=>$answer->question])->one();
        return $this->render('index', [
            "question" => $question,
            "answer" => $answer,
        ]);
    }
}
