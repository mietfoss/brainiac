<?php

use yii\helpers\Html;
//use yii\jui\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = Yii::t('app', 'Создать новый вопрос', [
    'modelClass' => 'Questions',
]);
$this->params['breadcrumbs'][] = ['label' => "Администрирование", 'url' => ['/manager']];
$this->params['breadcrumbs'][] = ['label' => "Вопросы", 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
