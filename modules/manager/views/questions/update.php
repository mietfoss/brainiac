<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = Yii::t('app', 'Изменение вопроса от ') . ' ' . $model->date . " (ID" . $model->id . ")";
$this->params['breadcrumbs'][] = ['label' => "Администрирование", 'url' => ['/manager']];
$this->params['breadcrumbs'][] = ['label' => "Вопросы", 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->date, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="questions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
