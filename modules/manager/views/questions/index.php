<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Управление вопросами');
$this->params['breadcrumbs'][] = ['label' => "Администрирование", 'url' => ['/manager']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить вопрос', [
    'modelClass' => 'Questions',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => "Дата",
                'value' => function($data) {
                    return Html::a(Html::encode($data->date), ['questions/view/', 'id'=>$data->id]);
                },
            'format' => 'raw'
            ],
            'text:ntext',
            'answer:ntext',
            'value',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
