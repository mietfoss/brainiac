<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */

$this->title = $model->date;
$this->params['breadcrumbs'][] = ['label' => "Администрирование", 'url' => ['/manager']];
$this->params['breadcrumbs'][] = ['label' => "Вопросы", 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-view">

    <h1>Вопрос от <?= Html::encode($model->date) ?> (ID<?= Html::encode($model->id);?>)</h1>

    <p>
        <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Точно хотите удалить этот вопрос?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'text:ntext',
            'answer:ntext',
            'value',
        ],
    ]) ?>

</div>
