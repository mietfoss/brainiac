<?php
use yii\helpers\Url;
?>
<style type="text/css">
    table.admin-menu {
        border: 0px;
    }
    table.admin-menu td {
        width: 300px;
        height: 300px;
        text-align: center;
        vertical-align: middle;
        border-radius: 10px;
    }
    a.admin-menu-item {
        text-decoration: none;
    }
    span.admin-nav-icon {
        font-size: 100px;
        color: inherit !important;
    }
</style>

<h1 style="text-align: center">Админ-панель</h1>
<table class="admin-menu">
    <tr>
    <td>
        <a class="admin-menu-item" href="<?=Url::to(['/'])?>">
            <span class="glyphicon glyphicon-home admin-nav-icon"></span>
            <h1>На главную</h1>
        </a>
    </>
    <td>
        <a class="admin-menu-item" href="<?=Url::to(['users/'])?>">
            <span class="glyphicon glyphicon-user admin-nav-icon"></span>
            <h1>Проверка вопросов</h1>
        </a>
    </td>
        <td>
            <a class="admin-menu-item" href="<?=Url::to(['questions/'])?>">
                <span class="glyphicon glyphicon-question-sign admin-nav-icon"></span>
                <h1>Управление вопросами</h1>
            </a>
        </td>
    </tr>
</table>