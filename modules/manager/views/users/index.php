<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Management'), 'url' => ['/manager']];
?>
<style type="text/css">
    p.question {
        color: #AFAFAF;
        font-style: italic;
    }
</style>

<div>
    <p class="question">
        Вопрос: <?=Html::encode($question->text)?>
    </p>
</div>

<div>
    <h4>Ответ участника <a href="https://vk.com/id<?=$answer->user_id?>"><?=$answer->user_id; ?></a></h4>
    <p class="answer">
        <?=Html::encode($answer->answer)?>
    </p>
</div>

<div>

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::activeHiddenInput($answer, 'id') ?>
    <?= Html::activeHiddenInput($answer, 'user_id') ?>
    <?= Html::activeHiddenInput($answer, 'question') ?>
    <?= Html::activeHiddenInput($answer, 'answer') ?>
    <?= Html::submitButton('Правильно', ['class' => 'btn btn-success', 'name' => 'Answers[correct]', 'value' => '1']) ?>
    <?= Html::submitButton('Неправильно', ['class' => 'btn btn-danger', 'name' => 'Answers[correct]', 'value' => '0']) ?>

    <?php ActiveForm::end(); ?>
</div>